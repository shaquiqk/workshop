package com.shaquiqlalu.workshopapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import com.shaquiqlalu.workshopapp.databinding.ActivityRegisterBinding;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityRegisterBinding binding;
    String username,password,confirmPass;
    private static String USER_NAME = "username";
    private static String PASSWORD = "password";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_register);

        binding.btnRegister.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_register:
                username = binding.edtUsername.getText().toString();
                password = binding.edtPassword.getText().toString();
                confirmPass = binding.edtConfirmPassword.getText().toString();

                if (password.equals(confirmPass)) {
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("Reg",MODE_PRIVATE);
                    SharedPreferences.Editor edit = preferences.edit();
                    edit.putString(USER_NAME, username);
                    edit.putString(PASSWORD,password);
                    edit.apply();
                    Toast.makeText(this, "Register Successful", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this,LoginActivity.class));
                    finish();
                }else {
                    binding.edtConfirmPassword.setError("Password not Correct");
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
