package com.shaquiqlalu.workshopapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.shaquiqlalu.workshopapp.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityLoginBinding binding;
    String name,pass;
    Dialog dialog;
    private static String USER_NAME = "username";
    private static String PASSWORD = "password";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login);


        binding.signup.setOnClickListener(this);
        binding.btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signup:
                startActivity(new Intent(this,RegisterActivity.class));
                break;
            case R.id.btn_login:
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("Reg",MODE_PRIVATE);
                String username = sharedPreferences.getString(USER_NAME, ".");
                String password = sharedPreferences.getString(PASSWORD,".");
                name = binding.edtUsername.getText().toString();
                pass = binding.edtPassword.getText().toString();
                Log.e("name",username);
                Log.e("pass",password);
                if (name.equals(username) && pass.equals(password)){
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }else {
                    Toast.makeText(LoginActivity.this, "Incorrect Credentials, Please try again...", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
